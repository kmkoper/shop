package pl.codementors.data_access.app_config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api")
public class DataAccessApplicationConfig extends Application {
}
