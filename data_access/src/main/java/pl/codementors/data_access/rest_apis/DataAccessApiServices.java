package pl.codementors.data_access.rest_apis;

import org.apache.commons.codec.digest.DigestUtils;
import pl.codementors.shop.model.Order;
import pl.codementors.shop.model.Product;
import pl.codementors.shop.model.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Stateless
@Path("/dataaccess")
public class DataAccessApiServices {

    @PersistenceContext
    EntityManager em;

    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String healthCheck() {
        return "Data access api is alive and reachable";
    }

    @Path("/user/{login}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public User findLogin(@PathParam("login") String login) {
        return em.createQuery("select a from User a where a.login = :login", User.class).setParameter("login", login).getSingleResult();
    }

    @Path("/users")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> findAllUsers () {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        query.from(User.class);
        List<User> allUsersList = em.createQuery(query).getResultList();
        return allUsersList;
    }

    @Path("/users/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public User findUser (@PathParam("id") int id) {
        return em.find(User.class, id);
    }

    @Path("/new_user")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User addUser(User user) {
        em.persist(user);
        return user;
    }

    @Path("/update_user")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateUser(User user) {
        em.merge(user);
    }

    @Path("/remove_user")
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void removeUser(User user) {
        em.remove(em.merge(user));
    }

    @Path("/orders")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Order> findAllOrders(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Order> query = cb.createQuery(Order.class);
        query.from(Order.class);
        List<Order> allOrdersList = em.createQuery(query).getResultList();
        return allOrdersList;
    }

    @Path("/orders/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Order findOneOrder(@PathParam("id") int id){
        return em.find(Order.class, id);
    }

    //not working
    @Path("/orders/users/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Order> getOrderListOfProvidedUser(@PathParam("id") int id){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Order> query = cb.createQuery(Order.class);
        query.from(Order.class);
        return em.createQuery("select o from Order o where o.user.id = :id").setParameter("id", id).getResultList();
    }

//    @Path("/orders")
//    @POST
//    @Consumes(MediaType.APPLICATION_JSON)
//    public void saveOrder(Order order){
//        em.persist(order);
//    }

    @Path("/orders")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateOrder(Order order){
        em.merge(order);
    }

    @Path("/products")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Product> findAllProducts(){
        return em.createQuery("select p from Product p").getResultList();
    }

    @Path("/products/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Product findOneProduct(@PathParam("id") int id) {
        return em.find(Product.class, id);
    }

    @Path("/products")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void saveProduct(Product product) {
        em.persist(product);
    }

    @Path("/products")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateProduct(Product product) {
        em.merge(product);
    }

    @Path("/products/{id}")
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteProduct(@PathParam("id") int id) {
        em.remove(findOneProduct(id)); //1. find 2. remove founded entity
    }
}
