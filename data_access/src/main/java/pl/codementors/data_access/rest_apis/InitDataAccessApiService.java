package pl.codementors.data_access.rest_apis;

import pl.codementors.shop.consul.ConsulRegistry;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;


@Singleton
@Startup
public class InitDataAccessApiService {

    private final static String NAME = "dataaccess";

    @Inject
    private ConsulRegistry registry;

    @PostConstruct
    public void init() {
        registry.registerService(NAME, getUri());
    }

    @PreDestroy
    public void destroy() {
        registry.deregisterService(NAME, getUri());
    }

    private String getUri() {
        return "http://localhost:8080/shop-dataaccess/api/dataaccess";
    }
}
