package pl.codementors.shop.consul;

import com.ecwid.consul.v1.ConsulClient;
import com.ecwid.consul.v1.agent.model.NewService;
import com.ecwid.consul.v1.agent.model.Service;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class ConsulRegistry {
    /**
     * Register service in the consul server.
     *
     * @param name name of the service
     * @param uri  service uri
     */
    public void registerService(String name, String uri) {
        try {
            URL url = new URL(uri);

            NewService newService = new NewService();
            newService.setId(serviceId(name, url.getHost(), url.getPort()));
            newService.setName(name);
            newService.setAddress(url.getHost());
            newService.setPort(url.getPort());
            Map<String, String> meta = new HashMap<>();
            meta.put("path", url.getPath());
            newService.setMeta(meta);

            NewService.Check serviceCheck = new NewService.Check();
            serviceCheck.setHttp(uri);
            serviceCheck.setInterval("10s");
            newService.setCheck(serviceCheck);

            getConsulClient().agentServiceRegister(newService);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Deregister service from server
     *
     * @param name name of service
     * @param uri  service uri
     */
    public void deregisterService(String name, String uri) {
        try {
            URL url = new URL(uri);
            getConsulClient().agentServiceDeregister(serviceId(name, url.getHost(), url.getPort()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates service id from name and url.
     *
     * @param name    name of the service
     * @param address address of service
     * @param port    port of service
     * @return created unique service id
     */
    private String serviceId(String name, String address, int port) {
        return name + ":" + address + ":" + port;
    }

    /**
     * @return Connection to consul client.
     */
    private ConsulClient getConsulClient() {
        return new ConsulClient("localhost");
    }

    /**
     * @param name service name
     * @return String with service address
     */
    public String discoverServiceUri(String name) {
        Map<String, Service> agentServices = getConsulClient().getAgentServices().getValue();

        Service match = null;

        for (Map.Entry<String, Service> entry : agentServices.entrySet()) {
            if (entry.getValue().getService().equals(name)) {
                match = entry.getValue();
                break;
            }
        }
        if (null == match) {
            throw new RuntimeException("Service " + name + " cannot be found");
        }
        return "http://" + match.getAddress() + ":" + match.getPort() + match.getMeta().get("path");
    }

    /**
     * @param name name of the service
     * @return service endpoint
     */
    public WebTarget getService(String name) {
        return ClientBuilder.newClient().target(discoverServiceUri(name));
    }
}

