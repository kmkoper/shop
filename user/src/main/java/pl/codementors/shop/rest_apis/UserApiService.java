package pl.codementors.shop.rest_apis;


import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import pl.codementors.shop.consul.ConsulRegistry;
import pl.codementors.shop.model.User;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

@Stateless
@Path("/users")
public class UserApiService {
    private static final String USERROLE = "USER";
    private static final String DATAACCESS = "dataaccess";

    @Inject
    ConsulRegistry consulRegistry;

    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String healthCheck() {
        return "User api is alive and reachable";
    }

    private WebResource getService() {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        return client.resource(UriBuilder.fromUri("http://localhost:8080/shop-dataaccess/").build());
    }

    @Path("")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addNewUser(User user){
        user.setStatus(false);
        user.setRole(USERROLE);
        consulRegistry.getService(DATAACCESS).path("/users").request().post(Entity.json(user));

    }

}
