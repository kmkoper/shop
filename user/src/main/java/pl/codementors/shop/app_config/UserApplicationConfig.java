package pl.codementors.shop.app_config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api")
public class UserApplicationConfig extends Application {
}
