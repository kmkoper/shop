package pl.codementors.shop.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.StringJoiner;


@Entity
@Table(name = "users")
public class User {

    @Column(name = "names")
    private String name;

    @Column(name = "surnames")
    private String surname;

    @Column(name = "emails")
    private String email;

    @Column(name = "logins")
    private String login;

    @Column(name = "roles")
    private String role;

    @Column(name = "status")
    private boolean status;

    @Column(name = "passwords")
    private String password;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    public User() {
    }

    public User(String name, String surname, String email, String login, boolean status, String role, String password ) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.login = login;
        this.status = status;
        this.role = role;
        this.password = password;

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String accessLevel) {
        this.role = accessLevel;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return status == user.status &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(email, user.email) &&
                Objects.equals(login, user.login) &&
                role == user.role &&
                Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, email, login, role, status, password);
    }


    @Override
    public String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("surname='" + surname + "'")
                .add("email='" + email + "'")
                .add("login='" + login + "'")
                .add("accessLevel=" + role)
                .add("status=" + status)
                .add("password='" + password + "'")
                .add("id=" + id)
                .toString();
    }
}
