package pl.codementors.shop.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "orders")
public class Order {

    @Column(name = "addresses")
    private String address;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

//    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER)
//    private List<Product> orderedProducts;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    private User user;

    public Order() {
    }

    public Order(String address, User user) {
        this.address = address;
        this.user = user;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

//    public List<Product> getOrderedProducts() {
//        return orderedProducts;
//    }
//
//    public void setOrderedProducts(List<Product> orderedProducts) {
//        this.orderedProducts = orderedProducts;
//    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(address, order.address) &&
//                Objects.equals(orderedProducts, order.orderedProducts) &&
                Objects.equals(user, order.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, user);
    }

    @Override
    public String toString() {
        return "Order{" +
                "address='" + address + '\'' +
                ", id=" + id +
                ", user=" + user +
                '}';
    }
}
