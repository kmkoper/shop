package pl.codementors.shop.model;

import java.util.List;
import java.util.Objects;

public class Basket {
    String login;
    List<Product> listOfProducts;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public List<Product> getListOfProducts() {
        return listOfProducts;
    }

    public void setListOfProducts(List<Product> listOfProducts) {
        this.listOfProducts = listOfProducts;
    }

    public Basket() {
    }

    public Basket(String login, List<Product> listOfProducts) {
        this.login = login;
        this.listOfProducts = listOfProducts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Basket basket = (Basket) o;
        return Objects.equals(login, basket.login) &&
                Objects.equals(listOfProducts, basket.listOfProducts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, listOfProducts);
    }
}
