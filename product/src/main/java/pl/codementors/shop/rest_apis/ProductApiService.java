package pl.codementors.shop.rest_apis;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import pl.codementors.shop.consul.ConsulRegistry;
import pl.codementors.shop.model.Product;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

@Stateless
@Path("/products")
public class ProductApiService {
    private static final String DATAACCESS = "dataaccess";

    @Inject
    ConsulRegistry consulRegistry;

    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String healthCheck() {
        return "Products api is alive and reachable";
    }

    @Path("/all")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Product> allProducts(){
        Gson gson = new Gson();
        Type productType = new TypeToken<Collection<Product>>() {
        }.getType();
        return gson.fromJson(getWebResource().path("/products")
                .accept(MediaType.APPLICATION_JSON).get(String.class), productType);
    }


    private WebResource getWebResource() {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        return client.resource(consulRegistry.getService(DATAACCESS).getUri());
    }
}
