

create table users(
  id int AUTO_INCREMENT primary key,
  access_level int,
  emails varchar(100),
  logins varchar(100),
  names varchar(100),
  surnames varchar(100),
  passwords varchar(100),
  status bit
);



create table orders(
  id int auto_increment primary key,
  addresses text,
  customer_id int,
  foreign key (customer_id) references users (id)
);

create table products(
  id int auto_increment primary key,
  availability bit,
  descriptions text,
  names varchar(100),
  prices bigint(20),
  seller_id int,
  order_id int,
  foreign key (seller_id)  references users (id),
  foreign key (order_id)  references orders (id)
);