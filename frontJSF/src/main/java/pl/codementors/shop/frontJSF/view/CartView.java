package pl.codementors.shop.frontJSF.view;

import com.google.gson.Gson;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import pl.codementors.shop.consul.ConsulRegistry;
import pl.codementors.shop.model.Basket;
import pl.codementors.shop.model.Product;
import pl.codementors.shop.model.User;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Entity;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Named
@ViewScoped
public class CartView implements Serializable {
    private static final long serialVersionUID = 123124124L;
    private static final String ORDERS = "orders";
    private List<Product> cartList;
    private Gson gson = new Gson();
    private HttpClient httpClient;

    @Inject
    private ConsulRegistry consulRegistry;

    public List<Product> getCartList() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        List<Product> cartList = (List<Product>) sessionMap.get("cart");
        return cartList;
    }

    public void setCartList(List<Product> cartList) {
        this.cartList = cartList;
    }


    public void removeFromCart(Product product){
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        List<Product> cartList = (List<Product>) sessionMap.get("cart");
        cartList.remove(product);
        sessionMap.remove("cart");
        sessionMap.put("cart", cartList);
    }

    public void order() throws ClientProtocolException, IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        List<Product> cartList = (List<Product>) sessionMap.get("cart");
        String login = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
        Basket basket = new Basket(login, cartList);

        consulRegistry.getService(ORDERS).path("").request().post(Entity.json(basket));

//        httpClient = HttpClientBuilder.create().build();
//        HttpPost post = new HttpPost(consulRegistry.getService(ORDERS).getUri());
//        StringEntity postingString = new StringEntity(gson.toJson(basket));
//        post.setEntity(postingString);
//        post.setHeader("Content-type", "application/json");


        
    }
}
