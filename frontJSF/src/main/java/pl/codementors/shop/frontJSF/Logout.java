package pl.codementors.shop.frontJSF;

import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

@Named
@ViewScoped
public class Logout implements Serializable {
    public String logout() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "http://localhost:8080/shop/index.xhtml?faces-redirect=true";
    }
}
