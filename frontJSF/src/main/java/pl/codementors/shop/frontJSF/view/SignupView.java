package pl.codementors.shop.frontJSF.view;

import org.apache.commons.codec.digest.DigestUtils;
import pl.codementors.shop.consul.ConsulRegistry;
import pl.codementors.shop.model.User;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Entity;
import java.io.Serializable;

@Named
@ViewScoped
public class SignupView  implements Serializable {
    private static final long serialVersionUID = 513124124L;
    private final static String USERAPI = "users";


    @Inject
    private ConsulRegistry consulRegistry;

    private User user = new User();

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void addNewUser() {
        consulRegistry.getService(USERAPI).request().post(Entity.json(user));
    }
}
