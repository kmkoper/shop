package pl.codementors.shop.frontJSF.filter;

import pl.codementors.shop.model.User;

import javax.faces.context.FacesContext;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginFilter implements Filter {
    private static long maxAge = 86400 * 30; // 30 days in seconds

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession();
        String login =req.getRemoteUser();
        String path = req.getRequestURI().substring(req.getContextPath().length());

        if (path.contains("/j_security_check")){
            HttpServletResponse res = (HttpServletResponse) response;
            res.sendRedirect("http://localhost:8080/shop/secured/admin_user.xhtml");
        }

        if (path.contains("/secured/")) {
            if (login != null) {
                if (login != null && !login.equals("")) {
                    chain.doFilter(request, response);
                } else {
                    HttpServletResponse res = (HttpServletResponse) response;
                    res.sendRedirect("http://localhost:8080/shop/login.xhtml");
                }
            } else {
                HttpServletResponse res = (HttpServletResponse) response;
                res.sendRedirect("http://localhost:8080/shop/login.xhtml");
            }

        } else {
            chain.doFilter(request, response);
        }
        }



    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("Cache Filter started: ");

    }

    @Override
    public void destroy() {
    }
}