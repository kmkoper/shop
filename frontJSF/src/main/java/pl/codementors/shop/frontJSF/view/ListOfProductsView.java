package pl.codementors.shop.frontJSF.view;

import pl.codementors.shop.consul.ConsulRegistry;
import pl.codementors.shop.frontJSF.RoleView;
import pl.codementors.shop.model.Product;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.GenericType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Named
@ViewScoped
public class ListOfProductsView implements Serializable {
    private static final long serialVersionUID = 3L;
    private static final String PRODUCTS = "products";

    private List<Product> productsList;

    private List<Product> productsInCart = new ArrayList<>();

    @Inject
    private ConsulRegistry consulRegistry;



    public RoleView getRoleView() {
        return roleView;
    }

    @Inject
    private RoleView roleView;

    public List<Product> getProductsList() {
        if (productsList == null) {
            productsList = consulRegistry.getService(PRODUCTS).path("/all").request().get(new GenericType<List<Product>>() {
            });
        }
        return productsList;
    }

    public void addToCart(Product product){
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        List<Product> productsFromSession = (List<Product>) sessionMap.getOrDefault("cart", null);
        if (productsFromSession == null || productsFromSession.isEmpty()){
            productsInCart.clear();
            productsInCart.add(product);
            sessionMap.put("cart", productsInCart);
        } else if (!productsFromSession.contains(product)) {
                sessionMap.remove("cart");
                productsFromSession.add(product);
                sessionMap.put("cart", productsFromSession);
            }
        }
}
