package pl.codementors.shop.rest_apis;

import pl.codementors.shop.consul.ConsulRegistry;
import pl.codementors.shop.model.Basket;
import pl.codementors.shop.model.Order;
import pl.codementors.shop.model.Product;
import pl.codementors.shop.model.User;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

@Stateless
@Path("/orders")
public class OrderApiService {
    private static final String DATAACCESS = "dataaccess";

    @Inject
    ConsulRegistry consulRegistry;

    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String healthCheck() {
        return "Orders api is alive and reachable";
    }

    @Path("")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void saveOrder(Basket basket){
        User user = consulRegistry.getService(DATAACCESS).path("/user/" + basket.getLogin()).request().get(User.class);
        for (Product product: basket.getListOfProducts()) {
            Order order = new Order();
            order.setUser(user);
            consulRegistry.getService(DATAACCESS).path("/orders").request().post(Entity.json(order));
            System.out.println(order);
            product.setOrder(order);
            consulRegistry.getService(DATAACCESS).path("/products").request().put(Entity.json(product));
        }
    }


}
